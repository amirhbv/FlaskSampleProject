## Flask sample project

a simple flask project that can search in wikipedia

you can login with username 'admin' and password '1234'

you can search in home page and it will show you the wikipedia summary for that subject

### Built With

* [Flask](https://github.com/pallets/flask) - The Python micro web framework used

### Usage

clone https://gitlab.com/amirhbv/FlaskSampleProject.git
#### ubuntu
```sh
    export FLASK_APP=server.py
    flask run
```
#### windows
```batch
    set FLASK_APP=server.py
    flask run
```

it will be on localhost:5000/
