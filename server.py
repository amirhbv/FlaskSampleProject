from flask import Flask, render_template, json, request, redirect, url_for, session
import requests

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route('/login', methods=['POST','GET'])
def login():
    error = ''
    if request.method == 'POST' :
        username = request.form['username']
        password = request.form['password']
        if username != 'admin' or password != '1234' :
            error = 'Username or Password is incorrect!'
        else :
            session['username'] = username
            return redirect(url_for('main'))
    return render_template('login.html', error = error)


@app.route('/main')
def main(content = ''):
    if 'username' not in session:
        return redirect(url_for('login'))
    return render_template('main.html', content = content)

@app.route('/search')
def search():
    query = request.args['query']
    baseUrl = 'https://en.wikipedia.org/w/api.php'
    my_atts = {
        'prop': 'extracts',
        'action': 'query',
        'format': 'json',
        'inprop': 'protection|watchers',
        'exsentences': 2,
        'formatversion': 2,
        'exintro':'',
        'explaintext':'',
        'redirects':'',
        'titles': query,
    }
    resp = requests.get(baseUrl, params = my_atts)
    data = resp.json()
    contentSummary = data['query']['pages'][0]['extract']
    return main(contentSummary)

if __name__ == "__main__":
    app.run(port=5000)
